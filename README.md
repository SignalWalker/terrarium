# Ruin Architect

A game used to inform development of [flint](https://gitlab.com/SignalWalker/flint) and [vaal](https://gitlab.com/SignalWalker/vaal).

## Compile & Run

### Requirements

* Vulkan SDK
* [Rust](https://rust-lang.org)

### Compile

```
git clone https://gilab.com/SignalWalker/ruin_architect
cd ruin_architect
cargo run --release
```

## Controls

* Translate Camera
    * X: A, D
    * Y: Q, E
    * Z: W, S
* Rotate Camera
    * X: Numpad 8, 2
    * Y: Numpad 4, 6
    * Z: Numpad 7, 9
* Reset Camera Position: C
* Quit: Escape

## Settings

Settings can be changed in [meta.stn](data/meta.stn).

## TODO

* Ray tracing
* Lighting
    * Shadows
* Networking
* GUI
    * Get [architect](https://gitlab.com/SignalWalker/architect) working
    * Get [painter](https://gitlab.com/SignalWalker/painter) working
* Terrain Building
    * Save / Load
* Physics
    * Momentum
    * Interpolation
    * Collision
    * Gravity
    * Friction
    * ...
* Movement
* Animation
    * Load from Collada
    * Inverse kinematics
* Grappling Hook
* Motorcycle
* Sword