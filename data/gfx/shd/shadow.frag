#version 460
#extension GL_ARB_separate_shader_objects : enable
#define PI 3.1415926538

layout(location = 0) in vec2 f_uv;
layout(location = 1) in vec3 f_norm;
layout(location = 2) in vec3 f_color;
layout(location = 0) out vec4 uFragColor;

void main() {
    float light = asin(dot(f_norm, vec3(0, 1, 0)) / length(f_norm)) / PI;
    vec4 color = vec4(f_color, 1) * vec4(light, light, light, 1);
    if (color.w <= 0.01) {
        discard;
    }
    uFragColor = color;
}
