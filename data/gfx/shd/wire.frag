#version 460
#extension GL_ARB_separate_shader_objects : enable
#define PI 3.1415926538

layout(location = 0) in vec2 f_uv;
layout(location = 1) in vec3 f_norm;
layout(location = 2) in vec3 f_color;
layout(location = 0) out vec4 uFragColor;

void main() {
    uFragColor = vec4(f_color, 1.0);
}
