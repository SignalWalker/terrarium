#![feature(vec_remove_item)]

#[macro_use]
extern crate specs_derive;

pub mod meta;
pub mod sand;

use crate::meta::{mason::MetaMason, Sim};
use ground_control::architect::{Architect, StoneMason};

use std::fs::File;
use std::io::BufReader;
use std::time::Instant;
use vaal::Runner;

fn main() {
    let init_time = Instant::now();
    let mut settings = {
        let file = File::open("data/meta.stn").unwrap();
        let bufread = BufReader::new(file);
        let mut res = MetaMason::new();
        let (mut arch, mut map) = Architect::from_buffer(bufread).unwrap();
        let _rem = res.handle_stones(&mut arch, &mut map);
        res
    };
    let (width, height) = {
        let settings = &mut settings.settings.control.settings;
        (
            settings
                .entry("width".to_string())
                .or_insert_with(|| "640".to_string())
                .parse()
                .unwrap(),
            settings
                .entry("height".to_string())
                .or_insert_with(|| "480".to_string())
                .parse()
                .unwrap(),
        )
    };
    let runner: Runner<Sim> = Runner::new(width, height);
    let sim = Sim::new(settings);
    eprintln!("Init Time: {:?}", init_time.elapsed());
    runner.run(sim);
    eprintln!("Dropping game: /('-')\\");
}
