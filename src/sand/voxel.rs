use na::Point3;
use std::collections::HashMap;
use std::iter::FromIterator;
use std::ops::Index;
use vaal::flint::lightcycle::na;
use vaal::physics::Position;

use vaal::physics::Momentum;
use vaal::render::Material;
use vaal::render::Shape;
use vaal::specs::prelude::*;

use nc::shape::{Ball, Plane, ShapeHandle};
use np::object::{BodyPartHandle, BodyStatus, ColliderDesc, RigidBodyDesc};
use np::object::{DefaultBodySet, DefaultColliderSet};
use vaal::ncollide3d as nc;
use vaal::nphysics3d as np;

#[derive(Debug, Default)]
pub struct SubstanceStorage(pub HashMap<String, SubstanceDef>);

impl FromIterator<SubstanceDef> for SubstanceStorage {
    fn from_iter<I: IntoIterator<Item = SubstanceDef>>(iter: I) -> Self {
        Self(HashMap::from_iter(
            iter.into_iter().map(|s| (s.name.clone(), s)),
        ))
    }
}

impl Index<&str> for SubstanceStorage {
    type Output = SubstanceDef;
    fn index(&self, s: &str) -> &Self::Output {
        &self.0[s]
    }
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct SubstanceDef {
    pub name: String,
    pub color: [f32; 3],
}

impl SubstanceDef {
    pub fn new<S: ToString>(name: S, color: [f32; 3]) -> Self {
        Self {
            name: name.to_string(),
            color,
        }
    }

    pub fn insert_new(&self, world: &World, shape: ShapeHandle<f32>, position: Point3<f32>) {
        let body_set = world.write_resource::<DefaultBodySet<f32>>();
        // let body_handle = world.translation();
        world
            .create_entity_unchecked()
            .with(Material::from("vox"))
            .with(Substance::from(self.name.clone()))
            .with(Shape::from("vox"))
            .build();
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default, Component)]
#[storage(DenseVecStorage)]
pub struct Substance(pub String);

impl<S: ToString> From<S> for Substance {
    fn from(s: S) -> Self {
        Self(s.to_string())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Component)]
#[storage(DenseVecStorage)]
pub struct RBody(pub np::object::DefaultBodyHandle);
