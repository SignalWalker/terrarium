use crate::sand::voxel::Substance;
use crate::sand::voxel::SubstanceStorage;
use vaal::TickCount;

use vaal::physics::Momentum;
use vaal::physics::Position;

use na::Vector3;
use vaal::flint::lightcycle::na;
use vaal::map::KdTree;
use vaal::specs::prelude::*;

const GRAVITY: [f32; 3] = [0.0, -9.81, 0.0]; // m/s/s
const AIR_DENSITY: f32 = 1.225; // kg/m^3
const CUBE_DRAG: f32 = 1.05;
const SPHERE_DRAG: f32 = 0.47;

mod wind;
pub use wind::*;

// Drag Equation:
// Fd = 0.5 * p * u^2 * Cd * A
// p: Fluid mass density
// u: Flow velocity
// Cd: Drag coefficient (Cube: 1.05)
// A: Area of orthographic projection of object to plane perpendicular to direction (Usually modeled as cross section)

#[derive(Debug)]
pub struct VoxelSim {
    pub min_airflow: f32,
}

// impl Default for VoxelSim {
//     fn default() -> Self {
//         Self::new()
//     }
// }

impl VoxelSim {
    pub fn new(min_airflow: f32) -> Self {
        Self { min_airflow }
    }
}

impl<'w> System<'w> for VoxelSim {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Read<'w, SubstanceStorage>,
        Read<'w, TickCount>,
        ReadExpect<'w, PressureField>,
        Entities<'w>,
        ReadStorage<'w, Substance>,
        WriteStorage<'w, Position>,
        WriteStorage<'w, Momentum>,
    );
    fn run(
        &mut self,
        (_sub_store, ticks, wind, ents, substances, mut positions, mut momentums): Self::SystemData,
    ) {
        use std::sync::{Arc, Mutex};
        let seconds = ticks.seconds();
        let gravity = Vector3::from(GRAVITY);

        let kd_points = Arc::new(Mutex::new(Vec::new()));
        let ent_tracker = Arc::new(Mutex::new(Vec::new()));
        (&substances, &ents, &mut positions, &mut momentums)
            .par_join()
            .for_each(|(_sub, ent, pos, mom)| {
                // let sub = sub_store[&sub.0];
                let mass = 1.0;
                // GRAVITY
                mom.0 += mass * gravity * seconds as f32;
                // AIR DRAG
                let air_flow: Vector3<f32> = wind.force_at(&pos.0) - (mom.0 / mass);
                let norm = air_flow.norm();
                if norm.abs() >= self.min_airflow {
                    let d_force: Vector3<f32> =
                        0.5 * AIR_DENSITY * norm.powf(2.0) * SPHERE_DRAG * (air_flow / norm);
                    mom.0 += d_force;
                }
                // POSITION
                pos.0 += (mom.0 / mass) * seconds as f32;

                pos.0.x = pos.0.x.min(11.0).max(0.0);
                pos.0.y = pos.0.y.min(11.0).max(0.0);
                pos.0.z = pos.0.z.min(11.0).max(0.0);

                let mut kd_points = kd_points.lock().unwrap();
                kd_points.push((pos.0, ent));
                let mut ent_tracker = ent_tracker.lock().unwrap();
                ent_tracker.push(ent);
            });
        let kd_tree = KdTree::construct(Arc::try_unwrap(kd_points).unwrap().into_inner().unwrap());
        let mut ent_tracker = Arc::try_unwrap(ent_tracker).unwrap().into_inner().unwrap();
        while !ent_tracker.is_empty() {
            let ent = ent_tracker.pop().unwrap();
            let mass = 1.0;
            let pos = positions.get(ent).unwrap().0;
            let mom = momentums.get(ent).unwrap().0;
            let (n_pos, n_ent) = kd_tree.nearest(&pos);
            if let Some((pfp, pfv, nfp, nfv)) = elastic_collision(
                seconds as f32,
                pos,
                mom / mass,
                *n_pos,
                momentums.get(*n_ent).unwrap().0,
            ) {
                positions.get_mut(ent).unwrap().0 = pfp;
                momentums.get_mut(ent).unwrap().0 = pfv * mass;
                positions.get_mut(*n_ent).unwrap().0 = nfp;
                momentums.get_mut(*n_ent).unwrap().0 = nfv * mass;
                // if let Some(i) = ent_tracker.iter().position(|v| v == n_ent) {
                //     ent_tracker.remove(i);
                // }
            }
        }
    }
}

type Vector3f = na::Vector3<f32>;
type Point3f = na::Point3<f32>;

fn elastic_collision(
    seconds: f32,
    p_pos: Point3f,
    p_vel: Vector3f,
    n_pos: Point3f,
    n_vel: Vector3f,
) -> Option<(Point3f, Vector3f, Point3f, Vector3f)> {
    use std::f32::EPSILON;
    let b_dist = na::distance(&p_pos, &n_pos);
    let rel_vel = p_vel + n_vel;
    if rel_vel.norm().abs() < std::f32::EPSILON {
        return None;
    }

    let np1 = Point3f::from(n_pos - p_pos);
    let nv1 = -(n_vel - p_vel);

    let (nst, nct, nsp, ncp) = {
        let theta = (np1.z / b_dist).acos();
        let phi = if np1.x.abs() <= EPSILON && np1.y.abs() <= EPSILON {
            0.0
        } else {
            np1.y.atan2(np1.x)
        };
        (theta.sin(), theta.cos(), phi.sin(), phi.cos())
    };

    let pvr = na::Vector3::new(
        (nct * ncp * nv1.x) + (nct * nsp * nv1.y) - (nst * nv1.z),
        (ncp * nv1.y) - (nsp * nv1.x),
        (nst * ncp * nv1.x) + (nst * nsp * nv1.y) - (nct * nv1.z),
    );
    let mut pvrf = pvr / rel_vel.norm();
    pvrf.z = pvrf.z.max(-1.0).min(1.0);
    let thetav = pvrf.z.acos();
    let phiv = if pvr.x.abs() <= EPSILON && pvr.y <= EPSILON {
        0.0
    } else {
        pvr.y.atan2(pvr.x)
    };

    let imp_norm = b_dist * thetav.sin(); // 1.0 == radius1 + radius2

    // No collision
    if thetav > std::f32::consts::PI / 2.0 || imp_norm.abs() > 1.0 {
        return None;
    }

    let col_time = (b_dist * thetav.cos() - (1.0 - imp_norm * imp_norm).sqrt()) / rel_vel.norm();

    // Not happening yet
    if col_time > seconds {
        return None;
    }

    let alpha = (-imp_norm).asin();
    let beta = phiv;
    let sbeta = beta.sin();
    let cbeta = beta.cos();

    let nfp: Point3f = np1 + nv1 * col_time + Vector3f::new(p_pos.x, p_pos.y, p_pos.z);
    let pfp = p_pos + (n_vel + nv1) * col_time;

    let va = (thetav + alpha).tan();

    let dvz2 = 2.0 * (pvr.z + va * (cbeta * pvr.x + sbeta * pvr.y)) / ((1.0 + va * va) * 2.0); // 2.0 = 1 + mass1 / mass2

    let nvr = na::Vector3::new(va * cbeta * dvz2, va * sbeta * dvz2, dvz2);
    let pvr = pvr - 2.0 * nvr;

    let pvf = na::Vector3::new(
        (nct * ncp * pvr.x) - (nsp * pvr.y) + (nst * ncp * pvr.z) + n_vel.x,
        (nct * nsp * pvr.x) + (ncp * pvr.y) + (nst * nsp * pvr.z) + n_vel.y,
        (nct * pvr.z) - (nst * pvr.x) + n_vel.z,
    );

    let nvf = na::Vector3::new(
        (nct * ncp * nvr.x) - (nsp * nvr.y) + (nst * ncp * nvr.z) + n_vel.x,
        (nct * nsp * nvr.x) + (ncp * nvr.y) + (nst * nsp * nvr.z) + n_vel.y,
        (nct * nvr.z) - (nst * nvr.x) + n_vel.z,
    );

    Some((pfp, pvf, nfp, nvf))
}
