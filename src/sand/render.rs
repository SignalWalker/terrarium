use crate::sand::voxel::Substance;
use crate::sand::voxel::SubstanceStorage;
use vaal::physics::Position;
use vaal::render::MatStorage;

use vaal::render::Static;

use flint::ash;
use flint::base::swapchain::SwapToken;

use flint::vertex::model::Model;
use vaal::flint;
use vaal::render::*;

use flint::lightcycle::na;
use na::Translation3;
use std::collections::HashMap;

use std::sync::Mutex;

use flint::base::VkData;
use rayon::prelude::*;
use vaal::specs::prelude::*;

pub struct VoxelRenderer {
    vox_model: Option<Model>,
}

impl Default for VoxelRenderer {
    fn default() -> Self {
        Self::new()
    }
}

impl VoxelRenderer {
    pub fn new() -> Self {
        Self { vox_model: None }
    }

    pub fn init(&mut self, world: &World) {
        if self.vox_model.is_none() {
            let vk = world.read_resource::<VkData>();
            let shapes = world.read_resource::<ShapeStorage>();

            self.vox_model.replace(Model::new(
                vk.device.clone(),
                &vk.device_memory_properties,
                shapes["vox"].clone(),
            ));
        }
    }
}

impl Renderer for VoxelRenderer {
    fn render(&self, world: &World, cmd_buf: ash::vk::CommandBuffer) {
        let cube = self
            .vox_model
            .as_ref()
            .expect("VoxelRenderer.render() called before init()");
        let mut mat_map = HashMap::new();
        (
            &world.read_storage::<Position>(),
            &world.read_storage::<Material>(),
            &world.read_storage::<Substance>(),
            !&world.read_storage::<Static>(),
        )
            .join()
            .for_each(|(pos, mat, sub, _not_stat)| {
                mat_map
                    .entry(mat.0.clone())
                    .or_insert_with(Vec::new)
                    .push((pos.clone(), sub.0.clone()))
            });
        cube.bind(cmd_buf);
        let swapchain = world.read_resource::<SwapToken>();
        let materials = world.read_resource::<MatStorage>();
        let dev = &world.read_resource::<VkData>().device;
        let sub_storage = world.read_resource::<SubstanceStorage>();
        for (mat, positions) in mat_map.iter() {
            let pipe = &swapchain.pipes[mat];
            pipe.bind(cmd_buf);
            let layout = pipe.layout;
            let cmd_buf = Mutex::new(cmd_buf);
            let push_consts = &materials[mat];
            positions.par_iter().for_each(|(pos, sub)| {
                let color = &(*sub_storage)[sub].color;
                let transform = Translation3::new(pos.0.x, pos.0.y, pos.0.z).to_homogeneous();
                {
                    let push = &push_consts["data"];
                    let cmd_buf = match cmd_buf.lock() {
                        Ok(c) => c,
                        Err(_) => return,
                    };
                    push.write(&*dev, *cmd_buf, layout, "trans", transform.as_slice());
                    push.write(&*dev, *cmd_buf, layout, "color", color);
                    cube.draw(*cmd_buf);
                }
            });
        }
    }
}

// impl<'w> System<'w> for VoxelRenderer {
//     #[allow(clippy::type_complexity)]
//     type SystemData = (
//         ReadExpect<'w, ash::Device>,
//         Read<'w, ash::vk::CommandBuffer>,
//         ReadExpect<'w, SwapToken>,
//         Read<'w, MatStorage>,
//         ReadStorage<'w, GridPos>,
//         ReadStorage<'w, Material>,
//         ReadStorage<'w, Static>,
//     );
//     fn run(
//         &mut self,
//         (dev, cmd_buf, swapchain, materials, positions, mats, statics): Self::SystemData,
//     ) {
//         unimplemented!()
//     }
// }
