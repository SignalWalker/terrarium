use specs::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::ops::Index;
use vaal::flint::lightcycle::na;
use vaal::specs;
use vaal::TickCount;

type Vector3f = na::Vector3<f32>;
type Point3f = na::Point3<f32>;

fn around(p: [usize; 3]) -> Vec<[usize; 3]> {
    HashSet::<[usize; 3]>::from_iter(vec![
        p,
        [p[0].saturating_sub(1), p[1], p[2]],
        [p[0].saturating_sub(1), p[1], p[2].saturating_sub(1)],
        [p[0], p[1], p[2].saturating_sub(1)],
        [p[0], p[1].saturating_sub(1), p[2]],
        [p[0].saturating_sub(1), p[1].saturating_sub(1), p[2]],
        [
            p[0].saturating_sub(1),
            p[1].saturating_sub(1),
            p[2].saturating_sub(1),
        ],
        [p[0], p[1].saturating_sub(1), p[2].saturating_sub(1)],
    ])
    .into_iter()
    .collect()
}

pub struct PressureField {
    pub width: usize,
    pub height: usize,
    pub depth: usize,
    pub field: Vec<f32>,
}

impl Index<&[usize; 3]> for PressureField {
    type Output = f32;
    fn index(&self, i: &[usize; 3]) -> &Self::Output {
        &self.field[i[2] * self.width * self.height + i[1] * self.width + i[0]]
    }
}

// impl IndexMut<&na::Point3<f32>> for WindField {
//     fn index_mut(&mut self, i: &na::Point3<f32>) -> &mut Self::Output {
//         let i = [i.x.trunc(), i.y.trunc(), i.z.trunc()];
//         &mut self.field[i[2] * self.width * self.height + i[1] * self.width + i[0]]
//     }
// }

impl PressureField {
    pub fn new(width: usize, height: usize, depth: usize) -> Self {
        Self {
            width,
            height,
            depth,
            field: (0..(width * height * depth))
                .map(|_p| 0.0)
                .collect::<Vec<_>>(),
        }
    }

    fn closest_corner(&self, p: &Point3f) -> [usize; 3] {
        [
            p.x.round().max(0.0).min(self.width as f32) as usize,
            p.y.round().max(0.0).min(self.height as f32) as usize,
            p.z.round().max(0.0).min(self.depth as f32) as usize,
        ]
    }

    pub fn force_at(&self, pos: &na::Point3<f32>) -> na::Vector3<f32> {
        const ROOT_3: f32 = 1.732_050_8;
        const SPHERE_SURF: f32 = 4.0 * std::f32::consts::PI;

        let mut res = Vector3f::zeros();
        let around = around(self.closest_corner(pos));
        for p in &around {
            let pf = Point3f::new(p[0] as f32 + 0.5, p[1] as f32 + 0.5, p[2] as f32 + 0.5);
            let weight = (1.0 - (na::distance(pos, &pf) / ROOT_3)).max(0.0);
            let dir = (pf - pos).normalize();
            res += weight * SPHERE_SURF * self[p] * dir;
        }
        res
    }
}

#[derive(Debug)]
pub struct PressureSystem {
    pub norm_speed: f32,
    pub norm_time: f64,
    time_acc: f64,
}

impl Default for PressureSystem {
    fn default() -> Self {
        Self::new(0.25, 3.0)
    }
}

impl PressureSystem {
    pub fn new(norm_speed: f32, norm_time: f64) -> Self {
        assert!(norm_speed >= 0.0 && norm_speed <= 1.0);
        assert!(norm_time >= 1.0);
        Self {
            norm_speed,
            norm_time,
            time_acc: 0.0,
        }
    }
}

impl<'w> System<'w> for PressureSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (Read<'w, TickCount>, WriteExpect<'w, PressureField>);

    fn run(&mut self, (ticks, mut field): Self::SystemData) {
        // self.time_acc += ticks.seconds();
        // if self.time_acc < self.norm_time {
        //     return;
        // }
        // let time = self.time_acc.trunc();
        // self.time_acc =
        self.time_acc += ticks.seconds();
        if self.time_acc < self.norm_time {
            // println!("Time Acc: {}\n", self.time_acc);
            return;
        }
        let time = self.time_acc;
        self.time_acc = 0.0;
        let mut tot_avg = 0.0;
        let c_weight = (1.0 - self.norm_speed) / time as f32;
        let mut new_field = Vec::with_capacity(field.field.len());
        for x in 0..field.width {
            for y in 0..field.height {
                for z in 0..field.depth {
                    let mut avg = 0.0;
                    let around = around([x, y, z]);
                    let o_weight = (1.0 - c_weight) / (around.len() - 1) as f32;
                    for p in &around {
                        avg += field[p] * if *p == [x, y, z] { c_weight } else { o_weight };
                    }
                    new_field.push(avg);
                    tot_avg += avg;
                }
            }
        }
        field.field = new_field;
        tot_avg /= field.field.len() as f32;
        println!("Average Pressure: {}", tot_avg);
        println!("Pressure Variance: {}\n\n", std_dev(&field.field, tot_avg));
    }
}

fn std_dev(vec: &[f32], mean: f32) -> f32 {
    vec.iter().fold(0.0, |a, v| a + (v - mean).powf(2.0)) / vec.len() as f32
}
