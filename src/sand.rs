use crate::sand::voxel::SubstanceStorage;
use na::Point3;
use na::Vector3;

use vaal::flint::lightcycle::na;
use vaal::specs::prelude::*;

use vaal::physics::*;
use vaal::render::Material;
use vaal::render::Shape;
use vaal::render::Static;

use nc::shape::{Ball, Plane, ShapeHandle};
use np::object::{BodyPartHandle, BodyStatus, ColliderDesc, RigidBodyDesc};
use np::object::{DefaultBodySet, DefaultColliderSet};
use vaal::ncollide3d as nc;
use vaal::nphysics3d as np;

pub mod voxel;

pub mod render;

pub mod sim;

pub struct Sandbox {
    pub size: (u16, u16, u16),
    was_init: bool,
    vox_shape: ShapeHandle<f32>,
}

impl Sandbox {
    pub fn new(width: u16, height: u16, depth: u16) -> Self {
        Self {
            size: (width, height, depth),
            was_init: false,
            vox_shape: ShapeHandle::new(Ball::new(1.0)),
        }
    }

    pub fn init(&mut self, world: &mut World) {
        if self.was_init {
            return;
        }
        self.was_init = true;
        let w = self.size.0 as f32;
        let h = self.size.1 as f32;
        let d = self.size.2 as f32;
        world
            .create_entity()
            .with(Material::from("wire"))
            .with(Static)
            .with(Position::from([w / 2.0, h / 2.0, d / 2.0]))
            .with(Rotation::default())
            .with(Scale(Vector3::new(-w, -h, -d)))
            .with(Shape::from("cube"))
            .build();

        let body_base = RigidBodyDesc::<f32>::new().status(BodyStatus::Static);
        let mut bodies = vec![
            body_base.clone().build(), // bottom
            body_base
                .clone()
                .rotation(Vector3::z() * std::f32::consts::PI)
                .translation(Vector3::y() * h)
                .build(), // top
            body_base
                .clone()
                .rotation(Vector3::z() * std::f32::consts::PI / 2.0)
                .translation(Vector3::x() * -w)
                .build(), // left
            body_base
                .clone()
                .rotation(Vector3::z() * -std::f32::consts::PI / 2.0)
                .translation(Vector3::x() * w)
                .build(), // right
            body_base
                .clone()
                .rotation(Vector3::x() * std::f32::consts::PI / 2.0)
                .translation(Vector3::z() * d)
                .build(), // front
            body_base
                .rotation(Vector3::x() * std::f32::consts::PI / 2.0)
                .translation(Vector3::z() * -d)
                .build(), // back
        ];
        let mut body_set = world.write_resource::<DefaultBodySet<f32>>();
        let mut collider_set = world.write_resource::<DefaultColliderSet<f32>>();
        let plane = ShapeHandle::<f32>::new(Plane::new(Vector3::y_axis()));
        for body in bodies.drain(0..) {
            let handle = body_set.insert(body);
            collider_set.insert(ColliderDesc::new(plane.clone()).build(BodyPartHandle(handle, 0)));
        }
    }

    pub fn spawn_voxel(&mut self, world: &World, pos: Point3<f32>, substance: &str) {
        (*world.read_resource::<SubstanceStorage>())[substance].insert_new(
            world,
            self.vox_shape.clone(),
            pos,
        );
    }
}
