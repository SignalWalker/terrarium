use crate::sand::render::VoxelRenderer;
use crate::sand::voxel::Substance;
use crate::sand::{
    sim::PressureField,
    voxel::{SubstanceDef, SubstanceStorage},
    Sandbox,
};
use ash::vk;
use vaal::render::Renderer;

use vaal::render::MatStorage;

use vaal::TickCount;

use vaal::render::StaticRenderer;

use flint::base::swapchain::SwapToken;
use flint::vertex::Polyhedron;
use std::iter::FromIterator;
use vaal::flint;
use vaal::render::{FreeCamera, ShapeStorage};
use vaal::Game;

use flint::winit;
use flint::{base::VkData, sampler::*, texture::*, *};

use vaal::specs::prelude::*;

use winit::{event::Event, event_loop::ControlFlow};

use flint::lightcycle::na::Matrix4;
use std::time::Instant;

pub mod mason;
use mason::*;

pub struct Sim<'w, 'wt> {
    cam_speed: f32,
    cam_rot: f32,
    sandbox: Sandbox,
    sim_disp: Option<Dispatcher<'w, 'wt>>,
    renderer: Option<Box<dyn Renderer>>,
    settings: MetaMason,
}

impl Sim<'_, '_> {
    pub fn new(settings: MetaMason) -> Self {
        Sim {
            cam_speed: 0.01,
            cam_rot: std::f32::consts::PI * 0.0005,
            sandbox: Sandbox::new(12, 12, 12),
            sim_disp: None,
            renderer: None,
            settings,
        }
    }
}

impl Game for Sim<'_, '_> {
    const NAME: &'static str = "Terrarium";
    const CLEAR_COLOR: [f32; 4] = [0.0, 0.0, 0.0, 0.8];
    const GRAVITY: [f32; 3] = [0.0, -9.81, 0.0];

    fn icon() -> Option<winit::window::Icon> {
        let image = match image::open("data/gfx/tex/icon.png") {
            Ok(o) => o,
            Err(_) => return None,
        }
        .to_rgba();
        let dims = image.dimensions();
        let data = image.into_raw();
        match winit::window::Icon::from_rgba(data, dims.0, dims.1) {
            Ok(o) => Some(o),
            Err(_e) => None,
        }
    }

    fn init(&mut self, mut world: &mut World) {
        world.register::<vaal::map::GridPos>();
        world.register::<vaal::render::Material>();
        world.register::<vaal::render::Static>();
        world.register::<vaal::render::Shape>();
        world.register::<vaal::physics::Momentum>();
        world.register::<vaal::physics::Position>();
        world.register::<vaal::physics::Rotation>();
        world.register::<vaal::physics::Scale>();
        world.register::<vaal::physics::AngularMomentum>();
        world.register::<crate::sand::voxel::Substance>();
        let mut shapes = ShapeStorage::from_iter(vec![
            ("tri", Polyhedron::tri()),
            ("quad", Polyhedron::quad()),
            ("ramp_tri", Polyhedron::ramp_tri()),
            ("ramp_quad", Polyhedron::ramp_quad()),
            ("cube", Polyhedron::cube()),
            ("vox", {
                let mut res = Polyhedron::load("data/gfx/mod/Sphere.dae");
                res.transform(&Matrix4::new_scaling(0.5));
                res
                // Polyhedron::point()
            }),
        ]);
        shapes.load("susanne", "data/gfx/mod/Susanne.dae");
        shapes.load("pawn", "data/gfx/mod/Pawn.dae");
        // shapes.load("vox", "data/gfx/mod/Sphere.dae");
        world.insert(shapes);
        world.insert(SubstanceStorage::from_iter(vec![SubstanceDef::new(
            "sand",
            [0.2, 0.6, 0.4],
        )]));

        let win_size = {
            let window = world.read_resource::<winit::window::Window>();
            let size = window.inner_size().to_physical(window.hidpi_factor());
            (size.width / size.height) as f32
        };
        world.insert(FreeCamera::new_persp(
            [0.0, 0.0, 0.0].into(),
            win_size,
            std::f32::consts::PI / 4.0,
            0.1,
            10000.0,
        ));

        self.sandbox.init(&mut world);

        let vk = world.remove::<VkData>().unwrap();
        let mut swapchain = world.remove::<SwapToken>().unwrap();

        let tex_time = Instant::now();
        world.insert(Texture::from_path(
            vk.device.clone(),
            &vk.device_memory_properties,
            swapchain.cmd_pool.buffers[0],
            vk.present_queue,
            "data/gfx/tex/circle.png",
        ));
        println!("Texture Load Time: {:?}", tex_time.elapsed());

        world.insert(SamplerToken::new(vk.device.clone()));

        {
            let mut materials = world.write_resource::<MatStorage>();

            // Pipeline
            materials.gen_material(
                &vk,
                &mut swapchain,
                "static",
                vec![
                    ("data/gfx/shd/static.vert", "main"),
                    ("data/gfx/shd/simple.frag", "main"),
                ],
                vk::PolygonMode::FILL,
            );

            materials.gen_material(
                &vk,
                &mut swapchain,
                "wire",
                vec![
                    ("data/gfx/shd/static.vert", "main"),
                    ("data/gfx/shd/wire.frag", "main"),
                ],
                vk::PolygonMode::LINE,
            );

            materials.gen_material(
                &vk,
                &mut swapchain,
                "vox",
                vec![
                    ("data/gfx/shd/vox.vert", "main"),
                    ("data/gfx/shd/shadow.frag", "main"),
                ],
                vk::PolygonMode::FILL,
            );
        }

        world.insert(vk);
        world.insert(swapchain);

        let mut s_renderer = StaticRenderer::default();
        s_renderer.gen_generic_meshes(&world);

        let mut v_renderer = VoxelRenderer::new();
        v_renderer.init(&world);

        self.renderer
            .replace(Box::new(s_renderer.chain(v_renderer)));

        // use rand::Rng;
        // let mut rng = rand::thread_rng();
        // for x in 0..12 {
        //     for z in 0..12 {
        //         self.sandbox.spawn_voxel(
        //             &world,
        //             [x as f32, rng.gen_range(8.0, 12.0), z as f32].into(),
        //             "sand",
        //         );
        //     }
        // }

        world.insert(PressureField::new(12, 12, 12));

        self.sim_disp.replace(
            DispatcherBuilder::new()
                .with(
                    crate::sand::sim::PressureSystem::new(0.01, 3.0),
                    "pressure",
                    &[],
                )
                .with(
                    crate::sand::sim::VoxelSim::new(0.01),
                    "voxel",
                    &["pressure"],
                )
                .build(),
        );
    }

    fn respond_to_event(&mut self, world: &mut World, event: Event<()>) {
        use winit::event::WindowEvent;
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input,
                        device_id: _device_id,
                        ..
                    },
                ..
            } => {
                #[cfg(debug_assertions)]
                println!("State: {:?}, ScanCode: {}", input.state, input.scancode);
                for action in self.settings.settings.control.handle_key(input) {
                    #[cfg(debug_assertions)]
                    println!("Action: {}", action);
                    match &action[..] {
                        "camera" => {
                            let mut camera = world.write_resource::<FreeCamera>();
                            *camera = FreeCamera::new([0.0, 0.0, 0.0].into(), camera.proj);
                            // dbg!(&camera);
                        }
                        "quit" => world.insert(ControlFlow::Exit),
                        "reload" => {
                            use rand::Rng;
                            let mut rng = rand::thread_rng();
                            for x in 0..12 {
                                for z in 0..12 {
                                    if !rng.gen::<bool>() {
                                        continue;
                                    }
                                    self.sandbox.spawn_voxel(
                                        &world,
                                        [x as f32, rng.gen_range(8.0, 11.0), z as f32].into(),
                                        "sand",
                                    );
                                }
                            }
                        }
                        "clear" => {
                            let voxels = {
                                let subs = world.read_component::<Substance>();
                                let ents = world.entities();
                                (&subs, &ents)
                                    .join()
                                    .map(|(_sub, ent)| ent)
                                    .collect::<Vec<_>>()
                            };
                            world.delete_entities(&voxels).unwrap();
                        }
                        "wind" => {
                            use rand::Rng;
                            const M_PRESSURE: f32 = 0.01;
                            let mut rng = rand::thread_rng();
                            let mut wind = world.write_resource::<PressureField>();
                            let len = wind.field.len() as f32;
                            for (i, vol) in wind.field.iter_mut().enumerate() {
                                let i_p = i as f32 / len;
                                let b = M_PRESSURE * -i_p;
                                let t = M_PRESSURE * (1.0 - i_p);
                                *vol += rng.gen_range(b, t);
                                // vec.x += rng.gen_range(-M_VEL, M_VEL);
                                // vec.y += rng.gen_range(-M_VEL, M_VEL);
                                // vec.z += rng.gen_range(-M_VEL, M_VEL);
                            }
                        }
                        _ => (),
                    }
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => world.insert(ControlFlow::Exit),
            Event::DeviceEvent { .. } => (),
            _ => (),
        }
    }

    fn simulate(&mut self, world: &mut World) {
        {
            let ticks = world.read_resource::<TickCount>();
            let cam_speed = (self.cam_speed as f64 * ticks.1) as f32;
            let cam_rot = (self.cam_rot as f64 * ticks.1) as f32;
            let mut camera = world.write_resource::<FreeCamera>();
            let up = camera.up;
            let right = camera.right();
            let forward = camera.forward;
            for action in &self.settings.settings.control.active {
                #[cfg(debug_assertions)]
                println!("Action: {}", action);
                match &action[..] {
                    "camera_up" => camera.mov(0.0, cam_speed, 0.0),
                    "camera_down" => camera.mov(0.0, -cam_speed, 0.0),
                    "camera_left" => camera.mov(-cam_speed, 0.0, 0.0),
                    "camera_right" => camera.mov(cam_speed, 0.0, 0.0),
                    "camera_in" => camera.mov(0.0, 0.0, cam_speed),
                    "camera_out" => camera.mov(0.0, 0.0, -cam_speed),
                    "camera_rot_left" => camera.rot(&(up * -cam_rot)),
                    "camera_rot_right" => camera.rot(&(up * cam_rot)),
                    "camera_rot_fore" => camera.rot(&(right * -cam_rot)),
                    "camera_rot_aft" => camera.rot(&(right * cam_rot)),
                    "camera_spin_left" => camera.rot(&(forward * cam_rot)),
                    "camera_spin_right" => camera.rot(&(forward * -cam_rot)),
                    _ => (),
                }
            }
        }
        self.sim_disp.as_mut().unwrap().dispatch(world);
    }

    fn init_render(&mut self, world: &World) {
        let mut swapchain = world.write_resource::<SwapToken>();
        let vk = world.read_resource::<VkData>();
        let mut camera = world.write_resource::<FreeCamera>();
        for (_k, pipe) in swapchain.pipes.iter_mut() {
            pipe.desc_pool.init_buffers(&vk.device_memory_properties);
            pipe.desc_pool
                .write_buffer("Camera", "cam", camera.fresh_mat().as_slice());
            pipe.desc_pool.refresh_desc_sets();
        }
    }

    fn render(&mut self, world: &World, cmd_buf: vk::CommandBuffer) {
        self.renderer.as_ref().unwrap().render(world, cmd_buf);
    }

    fn resized_swapchain(&mut self, world: &World, size: (f64, f64)) {
        world
            .write_resource::<FreeCamera>()
            .resize(size.0 as _, size.1 as _);
    }
}
